# UniFill

## Présentation du projet :

UniFill est un logiciel permettant à un ingénieur de pouvoir saisir ses temps sur ses différents projets.

Ce logiciel est composé d'une API permettant de collecter les données des projets et d'une Application sur Electron afin de pouvoir faire la saisie des temps.


## Schéma de la base de données utilisée :

![Capture1](https://gitlab.com/borkevo/unifill/-/blob/master/img/bdd.png)

## Technologies utilisées :

- API :
    - Express

- Logiciel :
    - React
    - Electron

- BDD :
    - MySql



