const express = require("express")
const morgan = require("morgan")
const bodyParser = require('body-parser')
const cors = require('cors')

require('dotenv').config()

const app = express()

var corsOptions = {
    origin: 'http://localhost:3000',
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(morgan('dev'))
app.use(bodyParser.urlencoded({extended : true}))
app.use(bodyParser.json())
app.use(cors(corsOptions))

const PORT = process.env.PORT || 3000

require('./routes/user.route')(app)
require('./routes/project.route')(app)
require('./routes/task.route')(app)
require('./routes/subtask.route')(app)
require('./routes/log.route')(app)

app.listen(PORT,()=>console.log(`http://localhost:${PORT}`))



