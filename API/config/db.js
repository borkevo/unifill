const mysql = require('mysql2')

const connection = mysql.createConnection({
    host : process.env.HOST,
    user: process.env.USER,
    password: process.env.PASSWORD,
    database : process.env.DB
})

connection.connect(error => {
    if (error) throw error
    console.log("Succesfully connected to the database")
})

module.exports = connection