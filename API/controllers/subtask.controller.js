const Subtask = require('../models/subtask.model')

exports.getAllSubtasks = (req,res)=>{
    Subtask.getAll((err,data)=>{
        if(err) res.status(500).send({
            message: err.message      
        })
        else res.send(data)
    })
}

exports.getInfo = (req,res)=>{
    Subtask.findById(req.params.id,(err,data)=>{
        if(err){
            if(kind==='not_found'){
                res.status(404).send({
                    message : 'Erreur 404 : Id Subtask not found'
                })
            }else{
                res.status(500).send({
                    message: err.message      
                })
            }
        }
        else res.send(data)
    })
}

exports.createSubtask = (req,res)=>{
    if(!req.body){
        res.status(400).send({
            message : "Les champs ne peuvent être vides"
        })
    }
    const subtask = new Subtask({name: req.body.name})
    Subtask.create(subtask,(err,data)=>
    {
        if (err) res.status(500).send({
            message: err.message
        })
        else res.send(data)
    })
}

exports.updateSubtask= (req,res)=>{
    if(!req.body){
        res.status(400).send({
            message : "Les champs ne peuvent être vides"
        })
    }
    Subtask.updateById(req.params.id, new Subtask(req.body), (err,data)=>{
        if(err){
            if(kind==='not_found'){
                res.status(404).send({
                    message : 'Erreur 404 : Id Subtask not found'
                })
            }else{
                res.status(500).send({
                    message: err.message      
                })
            }
        }
        else res.send(data)
    })
}

exports.deleteSubtask = (req,res)=>{
    Subtask.delete(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Aucuns tache avec l'ID ${req.params.id}.`
                });
            } else {
                res.status(500).send({
                  message: "Erreur lors de la suppression de la tache de l'id " + req.params.id
                });
              }
            } else res.send({ message: 'La tache a bien été supprimé', data: data });
        });
}