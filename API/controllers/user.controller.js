const User = require('../models/user.model')

exports.getAllUsers = (req,res)=>{
    User.getAll((err,data)=>{
        if(err) res.status(500).send({
            message: err.message      
        })
        else res.send(data)
    })
}

exports.getProfile = (req,res)=>{
    User.findById(req.params.id,(err,data)=>{
        if(err){
            if(kind==='not_found'){
                res.status(404).send({
                    message : 'Erreur 404 : Id user not found'
                })
            }else{
                res.status(500).send({
                    message: err.message      
                })
            }
        }
        else res.send(data)
    })
}

exports.createUser = (req,res)=>{
    if(!req.body){
        res.status(400).send({
            message : "Les champs ne peuvent être vides"
        })
    }
    const user = new User({email: req.body.email ,first_name: req.body.first_name, last_name: req.body.last_name, account_type: req.body.account_type})
    User.create(user,(err,data)=>
    {
        if (err) res.status(500).send({
            message: err.message
        })
        else res.send(data)
    })
}

exports.updateUser = (req,res)=>{
    if(!req.body){
        res.status(400).send({
            message : "Les champs ne peuvent être vides"
        })
    }
    User.updateById(req.params.id, new User(req.body), (err,data)=>{
        if(err){
            if(kind==='not_found'){
                res.status(404).send({
                    message : 'Erreur 404 : Id user not found'
                })
            }else{
                res.status(500).send({
                    message: err.message      
                })
            }
        }
        else res.send(data)
    })
}
exports.deleteUser = (req,res)=>{
    User.delete(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Aucuns user avec l'ID ${req.params.id}.`
                });
            } else {
                res.status(500).send({
                  message: "Erreur lors de la suppression du user de l'id " + req.params.id
                });
              }
            } else res.send({ message: 'Le user a bien été supprimé', data: data });
        });
}

exports.getAbsence = (req,res)=>{
    User.findAbsence(req.params.id,(err,data)=>{
        if(err){
            if(kind==='not_found'){
                res.status(404).send({
                    message : 'Erreur 404 : Id user not found'
                })
            }else{
                res.status(500).send({
                    message: err.message      
                })
            }
        }
        else res.send(data)
    })
}

exports.getSubtasks = (req,res)=>{
    User.findAffectedSubtasks(req.params.id,(err,data)=>{
        if(err){
            if(kind==='not_found'){
                res.status(404).send({
                    message : 'Erreur 404 : Id user not found'
                })
            }else{
                res.status(500).send({
                    message: err.message      
                })
            }
        }
        else res.send(data)
    })
}
    

