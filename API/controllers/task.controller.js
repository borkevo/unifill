const Task = require('../models/task.model')

exports.getAllTasks = (req,res)=>{
    Task.getAll((err,data)=>{
        if(err) res.status(500).send({
            message: err.message      
        })
        else res.send(data)
    })
}

exports.getInfo = (req,res)=>{
    Task.findById(req.params.id,(err,data)=>{
        if(err){
            if(kind==='not_found'){
                res.status(404).send({
                    message : 'Erreur 404 : Id task not found'
                })
            }else{
                res.status(500).send({
                    message: err.message      
                })
            }
        }
        else res.send(data)
    })
}

exports.createTask = (req,res)=>{
    if(!req.body){
        res.status(400).send({
            message : "Les champs ne peuvent être vides"
        })
    }
    const task = new Task({name: req.body.name})
    Task.create(task,(err,data)=>
    {
        if (err) res.status(500).send({
            message: err.message
        })
        else res.send(data)
    })
}

exports.updateTask= (req,res)=>{
    if(!req.body){
        res.status(400).send({
            message : "Les champs ne peuvent être vides"
        })
    }
    Task.updateById(req.params.id, new Task(req.body), (err,data)=>{
        if(err){
            if(kind==='not_found'){
                res.status(404).send({
                    message : 'Erreur 404 : Id task not found'
                })
            }else{
                res.status(500).send({
                    message: err.message      
                })
            }
        }
        else res.send(data)
    })
}

exports.deleteTask = (req,res)=>{
    Task.delete(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Aucuns tache avec l'ID ${req.params.id}.`
                });
            } else {
                res.status(500).send({
                  message: "Erreur lors de la suppression de la tache de l'id " + req.params.id
                });
              }
            } else res.send({ message: 'La tache a bien été supprimé', data: data });
        });
}

exports.getSubtasks = (req,res)=>{
    Task.findAllSubtasks(req.params.id,(err,data)=>{
        if(err){
            if(kind==='not_found'){
                res.status(404).send({
                    message : 'Erreur 404 : Id task not found'
                })
            }else{
                res.status(500).send({
                    message: err.message      
                })
            }
        }
        else res.send(data)
    })
}