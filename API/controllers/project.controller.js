const Project = require('../models/project.model')

exports.getAllProjects = (req,res)=>{
    Project.getAll((err,data)=>{
        if(err) res.status(500).send({
            message: err.message      
        })
        else res.send(data)
    })
}

exports.getInfo = (req,res)=>{
    Project.findById(req.params.id,(err,data)=>{
        if(err){
            if(kind==='not_found'){
                res.status(404).send({
                    message : 'Erreur 404 : Id project not found'
                })
            }else{
                res.status(500).send({
                    message: err.message      
                })
            }
        }
        else res.send(data)
    })
}

exports.createProject = (req,res)=>{
    if(!req.body){
        res.status(400).send({
            message : "Les champs ne peuvent être vides"
        })
    }
    const project = new Project({name: req.body.name})
    Project.create(project,(err,data)=>
    {
        if (err) res.status(500).send({
            message: err.message
        })
        else res.send(data)
    })
}

exports.updateProject = (req,res)=>{
    if(!req.body){
        res.status(400).send({
            message : "Les champs ne peuvent être vides"
        })
    }
    Project.updateById(req.params.id, new Project(req.body), (err,data)=>{
        if(err){
            if(kind==='not_found'){
                res.status(404).send({
                    message : 'Erreur 404 : Id project not found'
                })
            }else{
                res.status(500).send({
                    message: err.message      
                })
            }
        }
        else res.send(data)
    })
}

exports.deleteProject = (req,res)=>{
    Project.delete(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Aucuns project avec l'ID ${req.params.id}.`
                });
            } else {
                res.status(500).send({
                  message: "Erreur lors de la suppression du project de l'id " + req.params.id
                });
              }
            } else res.send({ message: 'Le project a bien été supprimé', data: data });
        });
}

exports.getWorker = (req,res)=>{
    Project.findAllWorker(req.params.id,(err,data)=>{
        if(err){
            if(kind==='not_found'){
                res.status(404).send({
                    message : 'Erreur 404 : Id project not found'
                })
            }else{
                res.status(500).send({
                    message: err.message      
                })
            }
        }
        else res.send(data)
    })
}

exports.getTasks = (req,res)=>{
    Project.findAllTasks(req.params.id,(err,data)=>{
        if(err){
            if(kind==='not_found'){
                res.status(404).send({
                    message : 'Erreur 404 : Id project not found'
                })
            }else{
                res.status(500).send({
                    message: err.message      
                })
            }
        }
        else res.send(data)
    })
}