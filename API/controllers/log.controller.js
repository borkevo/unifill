const Logs = require('../models/log.model')

exports.getAllLogs = (req,res)=>{
    Logs.getAll((err,data)=>{
        if(err) res.status(500).send({
            message: err.message      
        })
        else res.send(data)
    })
}

exports.getFive = (req,res)=>{
    Logs.getFiveLast(req.params.id,(err,data)=>{
        if(err){
            if(kind==='not_found'){
                res.status(404).send({
                    message : 'Erreur 404 : Id user not found'
                })
            }else{
                res.status(500).send({
                    message: err.message      
                })
            }
        }
        else res.send(data)
    })
}

exports.createLog = (req,res)=>{
    if(!req.body){
        res.status(400).send({
            message : "Les champs ne peuvent être vides"
        })
    }
    const log = new Log({name: req.body.name})
    Log.create(task,(err,data)=>
    {
        if (err) res.status(500).send({
            message: err.message
        })
        else res.send(data)
    })
}

exports.updateLog= (req,res)=>{
    if(!req.body){
        res.status(400).send({
            message : "Les champs ne peuvent être vides"
        })
    }
    Log.updateById(req.params.id, new Log(req.body), (err,data)=>{
        if(err){
            if(kind==='not_found'){
                res.status(404).send({
                    message : 'Erreur 404 : Id task not found'
                })
            }else{
                res.status(500).send({
                    message: err.message      
                })
            }
        }
        else res.send(data)
    })
}

exports.deleteLog = (req,res)=>{
    Log.delete(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Aucuns log avec l'ID ${req.params.id}.`
                });
            } else {
                res.status(500).send({
                  message: "Erreur lors de la suppression du log de l'id " + req.params.id
                });
              }
            } else res.send({ message: 'Le log a bien été supprimé', data: data });
        });
}