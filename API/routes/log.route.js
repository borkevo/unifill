module.exports = (app)=>
{
    const log = require('../controllers/log.controller.js')
    app.get('/logs', log.getAllLogs)
    app.get('/logs/:id', log.getFive)
    app.post('/logs', log.createLog)
    app.put('/logs/:id', log.updateLog)
    app.delete('/logs/:id', log.deleteLog)
}