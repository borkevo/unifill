module.exports = (app)=>
{
    const task = require('../controllers/task.controller.js')
    app.get('/tasks', task.getAllTasks)
    app.get('/tasks/:id', task.getInfo)
    app.post('/tasks',task.createTask)
    app.put('/tasks/:id', task.updateTask)
    app.delete('/tasks/:id', task.deleteTask)
    app.get('/allsubontask/:id',task.getSubtasks)
}