module.exports = (app)=>
{
    const subtask = require('../controllers/subtask.controller.js')
    app.get('/subtasks', subtask.getAllSubtasks)
    app.get('/subtasks/:id', subtask.getInfo)
    app.post('/subtasks',subtask.createSubtask)
    app.put('/subtasks/:id', subtask.updateSubtask)
    app.delete('/subtasks/:id', subtask.deleteSubtask)
    
}