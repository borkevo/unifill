module.exports = (app)=>
{
    const project = require('../controllers/project.controller.js')
    app.get('/projects',project.getAllProjects)
    app.get('/projects/:id',project.getAllProjects)
    app.post('/projects',project.createProject)
    app.put('/projects/:id', project.updateProject)
    app.delete('/projects/:id', project.deleteProject)
    app.get('/useronproject/:id', project.getWorker)
    app.get('/taskonproject/:id', project.getTasks)
}