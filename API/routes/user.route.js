module.exports = (app)=>
{
    const user = require('../controllers/user.controller.js')
    app.get('/users', user.getAllUsers)
    app.get('/users/:id', user.getProfile)
    app.post('/users',user.createUser)
    app.put('/users/:id',user.updateUser)
    app.delete('/users/:id',user.deleteUser)
    app.get('/absence/:id',user.getAbsence)
    app.get('/affected/:id',user.getSubtasks)
}