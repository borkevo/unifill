const db = require('../config/db')

const Task = function(task){
    this.from_date = task.from_date
    this.to_date = task.to_date
    this.name = task.name
    this.duration = task.duration
    this.statut = task.statut
    this.id_project = task.id_project
}

Task.getAll = result=>{
    db.query('Select * from tasks',(err,res)=>{
        if (err){
            console.log("ERROR :",err);
            result(null, err)
            return
        }
        console.log("\x1b[32m", "Les taches ont bien été fetch");
        result(null,res)
    })
}

Task.findById = (id,result) =>{
    db.query(`Select * from tasks where id_task = ${id} `,(err,res)=>{
        if (err){
            console.log("ERROR :",err)
            result(null, err)
            return;
        }
        if(res.length){
            console.log("gouanga :", res[0])
            result(null,res[0])
            return;
        }
        result({kind: 'not_found'},null)
        
    }) 
}

Task.create = (newTask, result)=>{
    db.query("INSERT INTO tasks SET ?", newTask, (err,res)=>
    {
        if (err){
            console.log("error : ", err)
            result(null, err)
            return;
        }
        console.log("Projet crée : ",{id: res.insertId, ...newTask})
        result(null, {id: res.insertId, ...newTask})
    })
}

Task.updateById = (id, task, result) =>{
    db.query('UPDATE tasks SET name = ?, from_date = ?, to_date=?, id_project = ?, duration = ?, statut = ? WHERE id_task = ?', [task.name, task.from_date, task.to_date, task.id_project, task.duration, task.statut, id], (err, res)=>{
        if (err) {
            console.log("error: ", err)
            result(null, err)
            return
        }
        if (res.affectedRows == 0) {
            // not found client with the id
            result({ kind: "not_found" }, null)
            return;
        }
        console.log("updated task: ", { id: id, ...task })
        result(null, { id: id, ...task })
    });
};

Task.delete = (id,result) =>{
    db.query(`DELETE FROM tasks WHERE id_task = ${id}`, (err, res)=>{
        if (err) {
            console.log("error: ", err)
            result(null, err)
            return
        }
        if (res.affectedRows == 0) {
            // not found client with the id
            result({ kind: "not_found" }, null)
            return
        }

        console.log("deleted task with id: ", id)
        result(null, res)
    })
}

Task.findAllSubtasks = (id,result) =>{
    db.query(`Select * from subtasks where id_task = ${id} `,(err,res)=>{
        if (err){
            console.log("ERROR :",err)
            result(null, err)
            return;
        }
        if(res.length){
            console.log("gouanga :", res)
            result(null,res) 
            return;
        }
        result(null,{kind: 'not_found'})
    }) 
}

module.exports = Task