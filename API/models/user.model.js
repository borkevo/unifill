const db = require('../config/db')

const User = function(user){
    this.email = user.email
    this.first_name = user.first_name
    this.last_name = user.last_name
    this.account_type = user.account_type
}

User.create = (newUser, result)=>{
    db.query("INSERT INTO users SET ?", newUser, (err,res)=>
    {
        if (err){
            console.log("error : ", err)
            result(null, err)
            return;
        }
        console.log("User crée : ",{id: res.insertId, ...newUser})
        result(null, {id: res.insertId, ...newUser})
    })
}

User.getAll = result =>{
    db.query("SELECT * FROM users",(err,res)=>{
        if (err){
            console.log("ERROR :",err);
            result(null, err)
            return
        }
        console.log("\x1b[32m", "Les users ont bien été fetch");
        result(null,res)
    })
}

User.findById = (id,result) =>{
    db.query(`Select * from users where id_user = ${id} `,(err,res)=>{
        if (err){
            console.log("ERROR :",err)
            result(null, err)
            return;
        }
        if(res.length){
            console.log("gouanga :", res[0])
            result(null,res[0])
            return;
        }
        result(null,{kind: 'not_found'})
        
    }) 
}

User.updateById = (id, user, result) =>{
    db.query('UPDATE users SET account_type = ?, email = ?, first_name = ?, last_name = ? WHERE id_user = ?', [user.account_type, user.email, user.first_name, user.last_name, id], (err, res)=>{
        if (err) {
            console.log("error: ", err)
            result(null, err)
            return
        }
        if (res.affectedRows == 0) {
            // not found client with the id
            result({ kind: "not_found" }, null)
            return;
        }
        console.log("updated user: ", { id: id, ...user })
        result(null, { id: id, ...user })
    });
};

User.delete = (id,result) =>{
    db.query(`DELETE FROM users WHERE id_user = ${id}`, (err, res)=>{
        if (err) {
            console.log("error: ", err)
            result(null, err)
            return
        }
        if (res.affectedRows == 0) {
            // not found client with the id
            result({ kind: "not_found" }, null)
            return
        }

        console.log("deleted user with id: ", id)
        result(null, res)
    })
}

User.findAbsence = (id,result) =>{
    db.query(`Select * from absence where id_user = ${id} `,(err,res)=>{
        if (err){
            console.log("ERROR :",err)
            result(null, err)
            return;
        }
        if(res.length){
            console.log("gouanga :", res)
            result(null,res)
            return
        }
        result(null,{kind: 'not_found'})
        
    })
}

User.findAffectedSubtasks = (id,result) =>{
    db.query(`Select s.name, s.from_date, s.to_date from affected_on_subtasks a join subtasks s on a.id_subtask=s.id_subtask where a.id_user = ${id} `,(err,res)=>{
        if (err){
            console.log("ERROR :",err)
            result(null, err)
            return;
        }
        if(res.length){
            console.log("gouanga :", res)
            result(null,res)
            return
        }
        result(null,{kind: 'not_found'})
        
    })
}

module.exports = User