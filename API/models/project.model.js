const db = require('../config/db')

const Project= function(project){
    this.name = project.name
}

Project.getAll = result =>{
    db.query('Select * from project',(err,res)=>{
        if (err){
            console.log("ERROR :",err);
            result(null, err)
            return
        }
        console.log("\x1b[32m", "Les projets ont bien été fetch");
        result(null,res)
    })
}

Project.findById = (id,result) =>{
    db.query(`Select * from project where id_project = ${id} `,(err,res)=>{
        if (err){
            console.log("ERROR :",err)
            result(null, err)
            return;
        }
        if(res.length){
            console.log("gouanga :", res[0])
            result(null,res[0])
            return;
        }
        result(null,{kind: 'not_found'})
        
    }) 
}

Project.create = (newProject, result)=>{
    db.query("INSERT INTO project SET ?", newProject, (err,res)=>
    {
        if (err){
            console.log("error : ", err)
            result(null, err)
            return;
        }
        console.log("Projet crée : ",{id: res.insertId, ...newProject})
        result(null, {id: res.insertId, ...newProject})
    })
}

Project.updateById = (id, project, result) =>{
    db.query('UPDATE project SET name = ? WHERE id_project = ?', [project.name, id], (err, res)=>{
        if (err) {
            console.log("error: ", err)
            result(null, err)
            return
        }
        if (res.affectedRows == 0) {
            // not found client with the id
            result({ kind: "not_found" }, null)
            return;
        }
        console.log("updated project: ", { id: id, ...project })
        result(null, { id: id, ...project })
    });
};

Project.delete = (id,result) =>{
    db.query(`DELETE FROM project WHERE id_project = ${id}`, (err, res)=>{
        if (err) {
            console.log("error: ", err)
            result(null, err)
            return
        }
        if (res.affectedRows == 0) {
            // not found client with the id
            result({ kind: "not_found" }, null)
            return
        }

        console.log("deleted project with id: ", id)
        result(null, res)
    })
}

Project.findAllWorker = (id,result) =>{
    db.query(`Select u.id_user, u.email, u.first_name, u.last_name from work_on_project w join users u on w.id_user=u.id_user where w.id_project = ${id} `,(err,res)=>{
        if (err){
            console.log("ERROR :",err)
            result(null, err)
            return;
        }
        if(res.length){
            console.log("gouanga :", res)
            result(null,res)
            return;
        }
        result(null,{kind: 'not_found'})
        
    }) 
}

Project.findAllTasks = (id,result) =>{
    db.query(`Select * from tasks where id_project = ${id} `,(err,res)=>{
        if (err){
            console.log("ERROR :",err)
            result(null, err)
            return;
        }
        if(res.length){
            console.log("gouanga :", res)
            result(null,res)
            return;
        }
        result(null,{kind: 'not_found'})
        
    })  
}

module.exports = Project