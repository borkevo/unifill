const db = require('../config/db')

const Log = function(log){
    this.id_user = log.id_user
    this.id_subtask = log.id_subtask
    this.duration = log.duration
    this.details = log.details
}

Log.getAll = result =>{
    db.query('Select * from logs',(err,res)=>{
        if (err){
            console.log("ERROR :",err);
            result(null, err)
            return
        }
        console.log("\x1b[32m", "Les logs ont bien été fetch");
        result(null,res)
    })
}

Log.getFiveLast = (id,result) =>{
    db.query(`Select l.id_log, s.name, l.details, l.duration from subtasks s join logs l on s.id_subtask=l.id_subtask join users u on l.id_user=u.id_user where l.id_user = ${id} ORDER BY l.id_log DESC LIMIT 5 `,(err,res)=>{
        if (err){
            console.log("ERROR :",err)
            result(null, err)
            return;
        }
        if(res.length){
            console.log("gouanga :", res)
            result(null,res)
            return;
        }
        result({kind: 'not_found'},null)
        
    }) 
}

Log.create = (newTask, result)=>{
    db.query("INSERT INTO logss SET ?", newLog, (err,res)=>
    {
        if (err){
            console.log("error : ", err)
            result(null, err)
            return;
        }
        console.log("Projet crée : ",{id: res.insertId, ...newLog})
        result(null, {id: res.insertId, ...newLog})
    })
}

Log.updateById = (id, log, result) =>{
    db.query('UPDATE logs SET id_user = ?, id_subtask = ?, details=?, duration = ?, WHERE id_log = ?', [log.id_user, log.id_subtask, log.details, log.duration, id], (err, res)=>{
        if (err) {
            console.log("error: ", err)
            result(null, err)
            return
        }
        if (res.affectedRows == 0) {
            // not found client with the id
            result({ kind: "not_found" }, null)
            return;
        }
        console.log("updated task: ", { id: id, ...log })
        result(null, { id: id, ...log })
    });
};

Log.delete = (id,result) =>{
    db.query(`DELETE FROM logs WHERE id_log = ${id}`, (err, res)=>{
        if (err) {
            console.log("error: ", err)
            result(null, err)
            return
        }
        if (res.affectedRows == 0) {
            // not found client with the id
            result({ kind: "not_found" }, null)
            return
        }

        console.log("deleted log with id: ", id)
        result(null, res)
    })
}

module.exports = Log