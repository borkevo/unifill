const db = require('../config/db')

const Subtask = function(subtask){
    this.from_date = subtask.from_date
    this.to_date = subtask.to_date
    this.name = subtask.name
    this.duration = subtask.duration
    this.statut = subtask.statut
    this.id_task = subtask.id_task
}

Subtask.getAll = result=>{
    db.query('Select * from subtasks',(err,res)=>{
        if (err){
            console.log("ERROR :",err);
            result(null, err)
            return
        }
        console.log("\x1b[32m", "Les sous taches ont bien été fetch");
        result(null,res)
    })
}

Subtask.findById = (id,result) =>{
    db.query(`Select * from subtasks where id_subtask = ${id} `,(err,res)=>{
        if (err){
            console.log("ERROR :",err)
            result(null, err)
            return;
        }
        if(res.length){
            console.log("gouanga :", res[0])
            result(null,res[0])
            return;
        }
        result(null,{kind: 'not_found'})
        
    }) 
}

Subtask.create = (newSubtask, result)=>{
    db.query("INSERT INTO subtasks SET ?", newSubtask, (err,res)=>
    {
        if (err){
            console.log("error : ", err)
            result(null, err)
            return;
        }
        console.log("Projet crée : ",{id: res.insertId, ...newSubtask})
        result(null, {id: res.insertId, ...newSubtask})
    })
}

Subtask.updateById = (id, subtask, result) =>{
    db.query('UPDATE subtasks SET name = ?, from_date = ?, to_date=?, id_task = ?, duration = ?, statut = ? WHERE id_subtask = ?', [Subtask.name, subtask.from_date, subtask.to_date, subtask.id_task, subtask.duration, subtask.statut, id], (err, res)=>{
        if (err) {
            console.log("error: ", err)
            result(null, err)
            return
        }
        if (res.affectedRows == 0) {
            // not found client with the id
            result({ kind: "not_found" }, null)
            return;
        }
        console.log("updated Subtask: ", { id: id, ...Subtask })
        result(null, { id: id, ...Subtask })
    });
};

Subtask.delete = (id,result) =>{
    db.query(`DELETE FROM subtasks WHERE id_subtask = ${id}`, (err, res)=>{
        if (err) {
            console.log("error: ", err)
            result(null, err)
            return
        }
        if (res.affectedRows == 0) {
            // not found client with the id
            result({ kind: "not_found" }, null)
            return
        }

        console.log("deleted Subtask with id: ", id)
        result(null, res)
    })
}

module.exports = Subtask